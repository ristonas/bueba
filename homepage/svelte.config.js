import preprocess from 'svelte-preprocess';
import netlify from '@sveltejs/adapter-netlify';
import { imagetools } from 'vite-imagetools';
// import { imageWidths } from './src/lib/imageWidths.js'; // TODO: wait for https://github.com/JonasKruckenberg/imagetools/issues/139 to be resolved
import path from 'path';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [
    preprocess({
      sourceMap: true,
      scss: {
        prependData: '@use "src/lib/styles/variables.scss" as *;',
      },
    }),
  ],

  kit: {
    adapter: netlify(),
    vite: {
      plugins: [
        imagetools(/*{
          // defaultDirectives: { width: imageWidths.join(';'), format: 'webp' },
        }*/),
      ],
      ssr: {
        noExternal: ['@iconify/svelte'],
      },
      resolve: {
        alias: {
          $images: path.resolve('./src/lib/assets/images'),
          $components: path.resolve('./src/lib/components'),
        },
      },
    },
  },
};

export default config;
