import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'imprint'; // Yes, the endpoint is named weirdly

export type Imprint = {
  text: MarkdownText;
  metadata: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  const rawBody = await response.json();

  const body: Imprint = {
    text: rawBody.text,
    metadata: rawBody.metadata,
  };

  return {
    status: response.status,
    body: body,
  };
};
