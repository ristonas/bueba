import type { Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'faqPage';

export type Faq = {
  Frage: string;
  Antwort: string;
};

export type FaqPage = {
  metadata: Metadata;
  faqs: Faq[];
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);
  const faqPage: FaqPage = await response.json();

  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }

  return {
    status: response.status,
    body: faqPage,
  };
};
