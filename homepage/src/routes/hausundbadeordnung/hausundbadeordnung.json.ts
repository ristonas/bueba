import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'houseRulesPage';

export type HouseRule = {
  ueberschrift: string;
  text: MarkdownText;
  anchor: string;
};

export type HouseRulesPage = {
  metadata: Metadata;
  badeordnungs: HouseRule[];
};

type RawHouseRulesPage = {
  metadata: Metadata;
  badeordnungs: {
    ueberschrift: string;
    text: string;
  }[];
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);
  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }

  const rawResponse: RawHouseRulesPage = await response.json();
  const parsedResponse: HouseRulesPage = {
    ...rawResponse,
    badeordnungs: rawResponse.badeordnungs.map((ele) => ({
      ueberschrift: ele.ueberschrift,
      anchor: ele.ueberschrift.replace(/[^\w]/g, ''),
      text: ele.text as MarkdownText,
    })),
  };
  return {
    status: response.status,
    body: parsedResponse,
  };
};
