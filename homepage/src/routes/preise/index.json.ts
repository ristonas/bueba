import type { RequestHandler } from '@sveltejs/kit';
import type { MarkdownText, Metadata } from '$lib/types';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'prices';

export type Fees = {
  introText: MarkdownText;
  individualFeesText: MarkdownText;
  adultFee: string;
  adultAdditionalFee: string | null;
  teenFee: string;
  teenAdditionalFee: string | null;
  childFee: string;
  childAdditionalFee: string | null;
  annualPassIntro: MarkdownText;
  annualPassText: string;
  annualPassFee: string;
  annualPassAdditionalFee: string | null;
  familyPassText: string;
  familyPassFee: string;
  familyPassAdditionalFee: string | null;
  annualPassLink: string;
  subscriptionIntroText: MarkdownText;
  subscriptionInfoText: MarkdownText;
  subscriptionFee: string;
  otherFeesText: MarkdownText;
  metadata?: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);
  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }

  const rawResponse = await response.json();

  return {
    status: response.status,
    body: rawResponse as Fees,
  };
};
