import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const home = 'home';

export type Sponsor = {
  name: string;
  link: string;
  logo: {
    url: string;
  };
};

export type Home = {
  introText: MarkdownText;
  newBuildingTitle: string;
  newBuildingText: MarkdownText;
  metadata: Metadata;
  sponsors: Sponsor[];
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${home}`);

  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }
  return {
    status: response.status,
    body: await response.json(),
  };
};
