import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'gallery';

type RawGalleryImage = {
  thumbnail: {
    url: string;
  };
  fullImage: {
    url: string;
  };
};

export type GalleryImage = {
  thumbnail: string;
  fullImage: string;
};

export type Schwimmbad = {
  equipmentDescription: MarkdownText;
  galleryImages: GalleryImage[];
  routeDescription: MarkdownText;
  googleMaps2ClickText: MarkdownText;
  accessibilityDisclaimer: MarkdownText;
  accessibilityText: MarkdownText;
  metadata: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);
  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }

  const rawBody = await response.json();
  const body: Schwimmbad = {
    galleryImages:
      (rawBody.galleryImages &&
        rawBody.galleryImages.map((it: RawGalleryImage) => ({
          thumbnail: it?.thumbnail?.url,
          fullImage: it?.fullImage?.url,
        }))) ||
      [],
    equipmentDescription: rawBody.description,
    routeDescription: rawBody.routeDescription,
    googleMaps2ClickText: rawBody.googleMaps2ClickText,
    accessibilityDisclaimer: rawBody.accessibilityDisclaimer,
    accessibilityText: rawBody.accessibilityText,
    metadata: rawBody.metadata,
  };

  return {
    status: response.status,
    body: body,
  };
};
