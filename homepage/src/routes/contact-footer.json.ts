import type { RequestHandler } from '@sveltejs/kit';
import type { Address } from '$lib/components/contact.svelte';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'contact';

export type Contact = {
  showPhoneNumber: boolean;
  telegramChannel: string;
  facebookLink: string;
  mapsLink: string;
  address: Address;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);
  if (response.status == 404) {
    // it's ok if this route isn't configured; return nothing to use default values
    return {
      status: 200,
      body: {},
    };
  }
  return {
    status: response.status,
    body: (await response.json()) as Contact,
  };
};
