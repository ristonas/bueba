import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'privacy';

export type PrivacyPolicy = {
  text: MarkdownText;
  metadata: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  const rawBody = await response.json();

  const body: PrivacyPolicy = {
    text: rawBody.text,
    metadata: rawBody.metadata,
  };

  return {
    status: response.status,
    body: body,
  };
};
