import type { Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'newsPage';

export type NewsPageItem = {
  metadata: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  return {
    status: response.status,
    body: await response.json(),
  };
};
