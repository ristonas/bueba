import type { RequestHandler } from '@sveltejs/kit';
import { convertNewsItem, RawNewsItem } from './index.json';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'neuigkeiten';

export const get: RequestHandler = async ({ params }) => {
  const response = await fetch(`${base}/${resource}/${params.articleId}`);

  if (response.ok) {
    const rawResponse: RawNewsItem = await response.json();

    return {
      status: response.status,
      body: convertNewsItem(rawResponse),
    };
  } else {
    return {
      status: response.status,
    };
  }
};
