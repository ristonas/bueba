import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'neuigkeiten/count';

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  return {
    status: response.status,
    body: await response.text(),
  };
};
