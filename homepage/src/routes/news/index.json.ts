import type { MarkdownText } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';
import type { Person } from '../team/index.json';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'neuigkeiten';

export type RawNewsItem = {
  id: number;
  title: string;
  article: string;
  author: Person;
  created_at: string;
  updated_at: string;
  image: {
    url: string;
  };
};

export type NewsItem = {
  id: number;
  title: string;
  publicationDate: Date;
  updateDate?: Date;
  article: MarkdownText;
  author: Person;
  imageUrl: string;
};

export const convertNewsItem = (rawNewsItem: RawNewsItem): NewsItem => ({
  article: rawNewsItem.article as MarkdownText,
  publicationDate: new Date(rawNewsItem.created_at),
  updateDate:
    rawNewsItem.created_at !== rawNewsItem.updated_at
      ? new Date(rawNewsItem.updated_at)
      : undefined,
  imageUrl: rawNewsItem.image?.url,
  author: rawNewsItem.author,
  id: rawNewsItem.id,
  title: rawNewsItem.title,
});

export const get: RequestHandler = async ({ url }) => {
  const response = await fetch(`${base}/${resource}?${url.searchParams.toString()}`);
  const rawResponse = await response.json();

  const body: NewsItem[] = rawResponse.map(convertNewsItem);

  return {
    status: response.status,
    body,
  };
};
