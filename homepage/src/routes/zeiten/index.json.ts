import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'swimming';

export type Swimming = {
  planExplanation: {
    explanationQuestion: string;
    answers: MarkdownText[];
  };
  planTitle: string;
  planDescription?: string;
  planImage?: string;
  courseDescription: MarkdownText;
  pdfDescription?: string;
  detailedPdf: string;
  faqText: MarkdownText;
  metadata: Metadata;
};

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  const rawBody = await response.json();

  const body: Swimming = {
    planExplanation: {
      ...rawBody.planExplanation,
      answers: rawBody.planExplanation.answers.map(
        (it: { id: number; answer: string }) => it.answer
      ),
    },
    planTitle: rawBody.title,
    planDescription: rawBody.description,
    planImage: rawBody.overview?.url,
    courseDescription: rawBody.beschreibungen,
    pdfDescription: rawBody.pdfDescription,
    detailedPdf: rawBody.detailedPdf?.url,
    faqText: rawBody.faqText,
    metadata: rawBody.metadata,
  };

  return {
    status: response.status,
    body: body,
  };
};
