import type { MarkdownText, Metadata } from '$lib/types';
import type { RequestHandler } from '@sveltejs/kit';

const base = import.meta.env.VITE_CMS_BASE_URL;
const resource = 'foerderverein';

type RawPerson = {
  name: string;
  firstName: string;
  email?: string;
  phoneNumber?: string;
  picture?: {
    url: string;
  };
};

export type Person = {
  name: string;
  firstName: string;
  email?: string;
  phone?: string;
  picture?: string;
};

export type Executive<T extends Person | RawPerson> = {
  person: T;
};

export type CommitteeMember<T extends Person | RawPerson> = {
  person: T;
  position: string;
};

export type Reason = {
  text: string;
};

export type Team = {
  introText: MarkdownText;
  executivesDescription: MarkdownText;
  executives: Executive<Person>[];
  foerdervereinDescription: MarkdownText;
  committeeDescription: MarkdownText;
  committeeMembers: CommitteeMember<Person>[];
  memberCount: number;
  reasons: Reason[];
  signupLink: string;
  metadata: Metadata;
};

export const rawPersonToPerson = ({ firstName, name, email, phoneNumber, picture }: RawPerson) => ({
  firstName,
  name,
  email,
  phoneNumber,
  picture: picture?.url,
});

export const get: RequestHandler = async () => {
  const response = await fetch(`${base}/${resource}`);

  const rawBody = await response.json();

  const body: Team = {
    introText: rawBody.introText,
    executivesDescription: rawBody.executivesDescription,
    executives: rawBody.executives.map((it: Executive<RawPerson>) => ({
      person: rawPersonToPerson(it.person),
    })),
    foerdervereinDescription: rawBody.foerdervereinDescription,
    committeeDescription: rawBody.committeeDescription,
    committeeMembers: rawBody.committeeMembers.map((it: CommitteeMember<RawPerson>) => ({
      person: rawPersonToPerson(it.person),
      position: it.position,
    })),
    reasons: rawBody.reasons,
    signupLink: rawBody.signupLink,
    memberCount: rawBody.memberCount,
    metadata: rawBody.metadata,
  };

  return {
    status: response.status,
    body: body,
  };
};
