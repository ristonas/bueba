/// <reference types="@sveltejs/kit" />

declare module '$lib/assets/images/*';
declare module '$images/*';

declare namespace svelte.JSX {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface HTMLAttributes<T> {
    // You can replace any with something more specific if you like
    onswipeleft?: () => void;
    onswiperight?: () => void;
  }
}
