import ResponsiveImage from './index.svelte';
import type ImageMetadata from './index.svelte';

const updateNode = (node: HTMLElement, params: ImageMetadata[] | string) => {
  node.style.position = 'relative';
  const props =
    typeof params == 'string'
      ? {
          sourceSet: params,
        }
      : {
          metadataSet: params,
        };
  new ResponsiveImage({ target: node, props: { background: true, alt: '', ...props } });
};

// Looks whack, but just calls updateNode with the HTML element this directive is used on. Just look above.
export function responsiveBg(
  node: HTMLElement,
  params: ImageMetadata[]
): { update?: (params: ImageMetadata[]) => void } {
  updateNode(node, params);
  return {
    update: (params: ImageMetadata[]) => {
      updateNode(node, params);
    },
  };
}
