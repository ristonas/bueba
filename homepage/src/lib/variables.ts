// Workaround for env variables due to https://timdeschryver.dev/blog/environment-variables-with-sveltekit

export const variables = {
  cmsBaseUrl: import.meta.env.VITE_CMS_BASE_URL,
};
