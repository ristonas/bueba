import watchMedia from 'svelte-media';
import cssBreakpoints from '$lib/styles/variables.scss';
import { parse as parseCss, Declaration, Rule } from 'css';

const parsedBreakpoints = parseCss(cssBreakpoints);
const exports: Rule | undefined = parsedBreakpoints?.stylesheet?.rules.find((rule: Rule) =>
  rule.selectors?.includes(':export')
);
const declarations: Declaration[] | undefined = exports?.declarations;

const getCssVariable = (name: string) =>
  declarations
    ?.find((declaration: Declaration) => declaration.property == name)
    ?.value?.replace(/"/g, '');

const returnDefaultAndLogError = (defaultValue?: string) => {
  console.error(
    "A media breakpoint was supposed to be extracted from CSS, but it didn't work. A default value of " +
      defaultValue +
      ' was used instead, which may not be consistent with the actual breakpoints!'
  );
  return defaultValue || '';
};

export const mediaBreakpoints = {
  mobile: getCssVariable('mobileMedia') || returnDefaultAndLogError(),
  tablet: getCssVariable('tabletMedia') || returnDefaultAndLogError(),
  desktop: getCssVariable('desktopMedia') || returnDefaultAndLogError(),
};

export const media = watchMedia({
  mobile: mediaBreakpoints.mobile,
  tablet: mediaBreakpoints.tablet,
  desktop: mediaBreakpoints.desktop,
});
