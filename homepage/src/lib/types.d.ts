/**
 * Can be made globally available by placing this
 * inside `global.d.ts` and removing `export` keyword
 */
export interface Locals {
  userid: string;
}

interface ImportMetaEnv {
  VITE_CMS_BASE_PATH: string;
}

export type Metadata = {
  title: string;
  metaDescription: string;
};

// This helps us wrap a primitive type that can't be used where the wrapped type is asked
// See https://stackoverflow.com/questions/26810574/is-there-a-way-to-create-nominal-types-in-typescript-that-extend-primitive-types
type Opaque<T, K> = T & { __opaque__: K };

// In our case, we want MarkdownText not to be confused with raw text
export type MarkdownText = Opaque<string, 'MarkdownText'>;
