export function swipeable(node: HTMLElement) {
  let x: number;
  let xDiff: number;

  function handleTouchStart(event: TouchEvent) {
    if (event.touches[0]) {
      x = event.touches[0].pageX;
      xDiff = 0;

      window.addEventListener('touchmove', handleTouchMove);
      window.addEventListener('touchend', handleTouchEnd);
      window.addEventListener('touchcancel', handleTouchEnd);
    }
  }

  function handleTouchMove(event: TouchEvent) {
    if (event.touches[0]) {
      const nextX = event.touches[0].pageX;
      xDiff = x - nextX;
    }
  }

  function handleTouchEnd() {
    if (xDiff > 100) {
      node.dispatchEvent(new CustomEvent('swiperight'));
    } else if (xDiff < -100) {
      node.dispatchEvent(new CustomEvent('swipeleft'));
    }

    window.removeEventListener('touchmove', handleTouchMove);
    window.removeEventListener('touchend', handleTouchEnd);
    window.removeEventListener('touchcancel', handleTouchEnd);
  }

  node.addEventListener('touchstart', handleTouchStart);

  return {
    destroy() {
      node.removeEventListener('touchstart', handleTouchStart);
    },
  };
}
