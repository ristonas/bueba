# Bürgerbad Handorf Website & CMS

Das ist das Repository fürs CMS und die Website vom Bürgerbad Handorf. Bis auf diese Einleitung ist das gesamte Projekt in englisch gehalten, um einen Denglischen Programmierstil zu vermeiden. Auch wenn das Projekt vermutlich niemals von nicht-deutsch-sprechenden Leuten verwaltet wird, finde ich, dass es einfach zum guten Stil gehört.

## Project Structure

The repository consists of two major components: `/cms` containing the CMS, done with Strapi, and `/homepage` for the actual homepage, built with Svelte. `/database` contains a test data dump and database helper scripts.

## Getting Started

The easiest way to get the project running is by executing `docker-compose up` in the root directory. This will bootstrap the application with all of the necessary components wired together and will populate a test database using the contents of `/database/test-data.dump`. See the site at `localhost:3000` and the CMS at `localhost:1337`. Everything should be hot-reloaded automatically.

### Updating test data

If `testdata.dump` was changed, the PSQL container won't re-run the init script because it already has data. `database/postgres-data` should have been created the first time the database has been initialized. Delete it (`sudo rm -rf` needed because of file permissions) and re-run `docker-compose up`.

### Dumping a new version of test data

To generate a new `testdata.dump` from the changes you've made in your local version of the database, run the following while the containers are running:

```bash
cd database
pg_dump -Fc bueba -U bueba -h localhost > testdata.dump
```

As of now, the password is `helo`.

## Usage with VSCode

To properly use VSCode with the Svelte part of the project, it is recommended to open /homepage as the workspace root. Otherwise, part of the Svelte tooling can't properly handle that the root is not at `/`. To interact with the other files, you can add the actual root folder using VSCode's workspace functionality (`File -> Add Folder to Workspace`).
