#!/bin/bash
set -e

pg_restore --verbose --clean --no-acl --no-owner -d "$POSTGRES_USER" -U "$POSTGRES_USER" -w /testdata.dump
