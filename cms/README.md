# Strapi Büba Handorf

## Use local database

```shell
cp .env.example .env
```

- Change USE_LOCAL_POSTGRES_DB to true
- Go to `../database`
- Execute `./launch-docker-postgres.sh` to start a local Postgres DB using Docker and to fill it with a test data dump
- `yarn develop`
