module.exports = ({ env }) => ({
  upload: {
    provider: "cloudinary",
    providerOptions: {
      cloud_name: env("CLOUDINARY_NAME"),
      api_key: env("CLOUDINARY_KEY"),
      api_secret: env("CLOUDINARY_SECRET"),
    },
    actionOptions: {
      upload: {},
      delete: {},
    },
  },
  "trigger-pipeline": {
    gitlabProjectId: "26742254",
    gitlabBranch: "develop",
    pipelineToken: env("GITLAB_PIPELINE_TOKEN"),
    pipelineReadToken: env("GITLAB_PIPELINE_READ_TOKEN"),
  },
});
