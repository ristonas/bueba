module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: !env("USE_LOCAL_POSTGRES_DB")
        ? {
            client: "sqlite",
            filename: env("DATABASE_FILENAME", ".tmp/data.db"),
          }
        : {
            client: "postgres",
            host: env("LOCAL_POSTGRES_HOST", "localhost"),
            port: 5432,
            database: "bueba",
            username: "bueba",
            password: "helo",
            ssl: false,
          },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
