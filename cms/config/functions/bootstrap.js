"use strict";

const pluralize = require("pluralize");

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = () => {
  // Add danish plural versions of collection names
  pluralize.addPluralRule("Sponsor", "Sponsoren");
  pluralize.addPluralRule("Vorstandsmitglied", "Vorstandsmitglieder");
  pluralize.addPluralRule("Badeordnung", "Badeordnungen");
  pluralize.addUncountableRule("FAQ");
  pluralize.addPluralRule("Neuigkeit", "Neuigkeiten");
  pluralize.addPluralRule("Person", "Personen");
};
