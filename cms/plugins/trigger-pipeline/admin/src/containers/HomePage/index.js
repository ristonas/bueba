import React, { memo, useState, useEffect, useRef } from "react";

import { Button, Padded, Text } from "@buffetjs/core";
import { Header } from "@buffetjs/custom";
import { LoadingBar } from "@buffetjs/styles";
import { faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useGlobalContext, request } from "strapi-helper-plugin";

import pluginId from "../../pluginId";

const POLL_INTERVAL = 10000;

const HomePage = () => {
  const { formatMessage } = useGlobalContext();

  const [pending, setPending] = useState(false);
  const [status, setStatus] = useState("unknown");

  useEffect(() => {
    let timeout;
    const checkBusy = async () => {
      const { status } = await getResult();

      setStatus(status);

      timeout = setTimeout(checkBusy, POLL_INTERVAL);
    };

    checkBusy();

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  const triggerPublish = async () => {
    setPending(true);
    await request(`/${pluginId}/trigger`, { method: "POST" });
    setStatus((await getResult()).status);
    setPending(false);
  };

  const getResult = async () => {
    return await request(`/${pluginId}/status`, { method: "GET" });
  };

  const getStatusVisuals = (status) => {
    const translation = formatMessage({
      id: `trigger-pipeline.home.pipeline.status.${status}`,
    });
    const map = {
      pending: (
        <>
          {translation} <LoadingBar />
        </>
      ),
      success: (
        <Text fontWeight="black" color="green">
          {translation}
        </Text>
      ),
      failed: (
        <Text fontWeight="black" color="red">
          {translation}
        </Text>
      ),
    };
    return map[status] ? (
      <>{map[status]}</>
    ) : (
      <Text fontWeight="black">{translation}</Text>
    );
  };

  return (
    <Padded size="md" top left bottom right>
      <Header
        title={{ label: formatMessage({ id: "trigger-pipeline.home.title" }) }}
        content={formatMessage({ id: "trigger-pipeline.home.description" })}
      />
      <Padded size="md" bottom>
        <Text>{formatMessage({ id: "trigger-pipeline.home.prompt" })}</Text>
      </Padded>
      <Button
        color="primary"
        icon={<FontAwesomeIcon icon={faUpload} />}
        onClick={triggerPublish}
        disabled={pending || status === "running" || status === "pending"}
      >
        {formatMessage({ id: "trigger-pipeline.home.button.trigger" })}
      </Button>
      <Padded color="primary" size="md" top bottom>
        <Text>{formatMessage({ id: "trigger-pipeline.home.statusText" })}</Text>
        {getStatusVisuals(status)}
      </Padded>
    </Padded>
  );
};

export default memo(HomePage);
