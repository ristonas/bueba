"use strict";

const axios = require("axios");

const pluginId = require("../admin/src/pluginId");

/**
 * trigger-pipeline.js controller
 *
 * @description: A set of functions called "actions" of the `trigger-pipeline` plugin.
 */

module.exports = {
  readStatus: async (ctx) => {
    const { gitlabProjectId, gitlabBranch, pipelineReadToken } = strapi.plugins[
      pluginId
    ].config;

    const headers = {
      "PRIVATE-TOKEN": pipelineReadToken,
    };

    const url = `https://gitlab.com/api/v4/projects/${gitlabProjectId}/pipelines?ref=${gitlabBranch}&per_page=1`;

    let status;

    try {
      const response = await axios.get(url, { headers });
      status = response.data[0].status;
    } catch (_error) {
      status = "unknown";
    }

    ctx.send({
      status,
    });
  },
  /**
   * Triggers the configured Pipeline
   * @param {*} ctx
   */
  trigger: async (ctx) => {
    const { gitlabProjectId, gitlabBranch, pipelineToken } = strapi.plugins[
      pluginId
    ].config;

    const body = {
      token: pipelineToken,
      ref: gitlabBranch,
    };

    const url = `https://gitlab.com/api/v4/projects/${gitlabProjectId}/trigger/pipeline`;

    let success;
    let pipelineId;

    try {
      const response = await axios.post(url, body);
      success = response.status === 201;
      pipelineId = response.data.id;
    } catch (error) {
      success = false;
    }

    ctx.send({
      success,
      pipelineId,
    });
  },
};
